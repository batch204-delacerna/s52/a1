import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function loginUser(e) {
    e.preventDefault();

    setEmail("");
    setPassword("");
    alert('Login successful!');
  }

  useEffect(() => {
    if((email !== '' && password !== '')) {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password]);

  return (
      <Form onSubmit={e => loginUser(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder = "Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label className="mt-3">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder = "Enter Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {isActive ?
        <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
          Login
        </Button>
        :
        <Button className="mt-3" variant="primary" id="submitBtn" disabled>
          Login
        </Button>
      }

      </Form>

  );

}